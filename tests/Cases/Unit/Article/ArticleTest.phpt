<?php declare(strict_types = 1);

namespace Tests\Cases\Unit\Article;

use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../../../vendor/autoload.php';

class ArticleTest extends TestCase
{

	/**
	 * @return mixed[][]
	 */
	protected function getHelloWorldData(): array
	{
		return [
			['Hello', 'Hello', true],
			['world!', 'world !', false],
		];
	}

	/**
	 * @dataProvider getHelloWorldData
	 */
	public function testHelloWorld(string $word1, string $word2, bool $equal): void
	{
		if ($equal) {
			Assert::equal($word1, $word2);
		} else {
			Assert::notSame($word1, $word2);
		}
	}

}

$test = new ArticleTest();
$test->run();
