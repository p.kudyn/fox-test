# FOX TEST

## Installation instructions

* Clone the repo, that's the easy part, right?
* Run `composer install` command in the root directory. If you don't have composer yet, then wake up dude!
* Adjust the elasticsearch settings in `config/config.neon`.
* Run `php bin/run.php` - in other words: Warp 9, engage!.
* See `var/log/import.log` for the process details.
* Enjoy!
