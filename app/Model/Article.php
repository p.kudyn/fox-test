<?php declare(strict_types = 1);

namespace App\Model;

class Article
{

	private string $title;

	private string $description;

	private string $url;

	public function __construct(string $title, string $description, string $url)
	{
		$this->title = $title;
		$this->description = $description;
		$this->url = $url;
	}

	public function getTitle(): string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getUrl(): string
	{
		return $this->url;
	}

	public function setUrl(string $url): self
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return mixed[]
	 */
	public function asArray(): array
	{
		return [
			'title' => $this->title,
			'description' => $this->description,
			'url' => $this->url,
		];
	}

}
