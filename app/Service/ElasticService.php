<?php declare(strict_types = 1);

namespace App\Service;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticService
{

	private array $config;

	private Client $client;

	/**
	 * @param mixed[] $config
	 */
	public function __construct(array $config)
	{
		$this->config = $config;
		$this->client = ClientBuilder::create()->setHosts($config['hosts'])->build();
	}

	public function getClient(): Client
	{
		return $this->client;
	}

}
