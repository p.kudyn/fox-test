<?php declare(strict_types = 1);

namespace App\Service;

use App\Model\Article;
use Throwable;
use Tracy\Debugger;
use Tracy\ILogger;

class ArticleService
{

	public const INDEX = 'article';

	private ElasticService $elastic;

	public function injectElasticService(ElasticService $elasticService): void
	{
		$this->elastic = $elasticService;
	}

	public function createIndex(): void
	{
		$params = [
			'index' => self::INDEX,
		];
		if (!$this->elastic->getClient()->indices()->exists($params)) {
			$this->elastic->getClient()->indices()->create($params);
		}
	}

	public function removeArticles(string $url): int
	{
		$params = [
			'index' => self::INDEX,
			'body'  => [
				'query' => [
					'match' => [
						'url' => $url,
					],
				],
			],
		];
		$articles = $this->elastic->getClient()->search($params);

		foreach ($articles['hits']['hits'] as $article) {
			$this->elastic->getClient()->delete(['id' => $article['_id'], 'index' => self::INDEX]);
		}

		return count($articles['hits']['hits']);
	}

	public function indexArticle(Article $article): ?string
	{
		$params = [
			'index' => self::INDEX,
			'body' => $article->asArray(),
		];
		$response = $this->elastic->getClient()->index($params);

		return $response['result'] === 'created' ? $response['_id'] : null;
	}

	/**
	 * @param Article[] $articles
	 * @return int - number of successfully indexed articles
	 */
	public function indexArticles(array $articles): int
	{
		$res = 0;
		foreach ($articles as $article) {
			try {
				$this->indexArticle($article);
				$res++;
			} catch (Throwable $e) {
				Debugger::log('Cannot index article: ' . $article->getTitle() . '; Exception: ' . $e->getMessage(), ILogger::ERROR);
			}
		}

		return $res;
	}

}
