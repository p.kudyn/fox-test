<?php declare(strict_types = 1);

namespace App\Service;

use App\Model\Article;

interface PageParser
{

	/**
	 * @return Article[]
	 */
	public function analyze(string $url): array;

}
