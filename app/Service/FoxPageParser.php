<?php declare(strict_types = 1);

namespace App\Service;

use App\Exception\ParserException;
use App\Model\Article;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\Node\HtmlNode;
use Throwable;
use Tracy\Debugger;
use Tracy\ILogger;

class FoxPageParser implements PageParser
{

	/**
	 * @return Article[]
	 */
	public function analyze(string $url): array
	{
		$dom = new Dom();
		$dom->loadFromUrl($url);

		$section = $this->getSection($dom);

		$articles = $section->find('article.functions-all__item');

		$res = [];
		foreach ($articles as $articleNode) {
			try {
				$article = $this->processArticle($articleNode, $url);
				if ($article !== null) {
					$res[] = $article;
				}
			} catch (Throwable $e) {
				Debugger::log('Cannot process article, exception: ' . $e->getMessage() . '; Node: ' . $articleNode->innerHtml, ILogger::ERROR);
			}
		}

		return $res;
	}

	protected function getSection(Dom $dom): HtmlNode
	{
		/** @var Dom\Node\Collection $sections */
		$sections = $dom->find('section.functions-all');

		if ($sections->count() === 0) {
			throw new ParserException('Section "functions-all" not found on the page.');
		}

		if ($sections->count() > 1) {
			throw new ParserException('More than one section "functions-all" found on the page.');
		}

		return $sections[0];
	}

	protected function processArticle(HtmlNode $articleNode, string $url): ?Article
	{
		$title = $this->getArticleTitle($articleNode);
		$description = $this->getArticleDescription($articleNode);

		if ($title === null || $description === null) {
			return null;
		}

		return new Article($title, $description, $url);
	}

	protected function getArticleTitle(HtmlNode $articleNode): ?string
	{
		$h4Node = $articleNode->find('h4');
		if ($h4Node->count() === 0) {
			Debugger::log('<h4> element not found in article: ' . $articleNode->innerhtml, ILogger::ERROR);
			return null;
		}

		if ($h4Node->count() > 1) {
			Debugger::log('More than one <h4> element found in article: ' . $articleNode->innerhtml, ILogger::ERROR);
			return null;
		}

		return $h4Node[0]->innerText;
	}

	protected function getArticleDescription(HtmlNode $articleNode): ?string
	{
		$pNode = $articleNode->find('p');
		if ($pNode->count() === 0) {
			Debugger::log('<p> element not found in article: ' . $articleNode->innerhtml, ILogger::ERROR);
			return null;
		}

		if ($pNode->count() > 1) {
			Debugger::log('More than one <p> element found in article in article: ' . $articleNode->innerhtml, ILogger::ERROR);
			return null;
		}

		return $pNode[0]->innerText;
	}

}
