<?php declare(strict_types = 1);

namespace App;

use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use Tracy\Debugger;

class App
{

	private Container $container;

	public function __construct()
	{
		$this->init();
		$this->container = $this->createContainer();
	}

	protected function init(): void
	{
		Debugger::enable(Debugger::PRODUCTION, __DIR__ . '/../var/log');
	}

	protected function createContainer(): Container
	{
		$loader = new ContainerLoader(__DIR__ . '/../var/temp');
		$class = $loader->load(function ($compiler): void {
			$compiler->loadConfig(__DIR__ . '/../config/config.neon');
		});
		/** @var Container $container */
		$container = new $class();

		return $container;
	}

	public function getContainer(): Container
	{
		return $this->container;
	}

}
