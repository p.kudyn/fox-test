<?php declare(strict_types = 1);

use App\App;
use App\Service\ArticleService;
use App\Service\FoxPageParser;
use Tracy\Debugger;

//Create application
require_once __DIR__ . '/../vendor/autoload.php';
$app = new App();

$logLevel = 'import';


//Run page parser
$url = $app->getContainer()->getParameters()['pageUrl'];
Debugger::log('Processing page: ' . $url, $logLevel);

/** @var FoxPageParser $parser */
$parser = $app->getContainer()->getByType(FoxPageParser::class);
$articles = $parser->analyze($url);

Debugger::log('Articles found: ' . count($articles), $logLevel);


//Index articles to Elastic
/** @var ArticleService $articleService */
$articleService = $app->getContainer()->getByType(ArticleService::class);

$articleService->createIndex();

$removed = $articleService->removeArticles($url);
Debugger::log('Old articles removed: ' . $removed, $logLevel);

$count = $articleService->indexArticles($articles);
Debugger::log('Indexed articles: ' . $count, $logLevel);

if ($count !== count($articles)) {
	Debugger::log('Warning: not all articles were successfully indexed!', $logLevel);
}

Debugger::log('Done.', $logLevel);
